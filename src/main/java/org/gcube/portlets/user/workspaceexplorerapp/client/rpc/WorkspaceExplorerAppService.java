package org.gcube.portlets.user.workspaceexplorerapp.client.rpc;

import java.util.List;

import org.gcube.portlets.user.workspaceexplorerapp.shared.FilterCriteria;
import org.gcube.portlets.user.workspaceexplorerapp.shared.Item;
import org.gcube.portlets.user.workspaceexplorerapp.shared.ItemType;
import org.gcube.portlets.user.workspaceexplorerapp.shared.WorkspaceNavigatorServiceException;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The Interface WorkspaceExplorerAppService.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Jul 3, 2017
 */
@RemoteServiceRelativePath("WorkspaceExplorerAppService")
public interface WorkspaceExplorerAppService extends RemoteService {

	/**
	 * Gets the folder.
	 *
	 * @param folder           the folder @param showableTypes the showable
	 *                         types @param purgeEmpyFolders the purge empy
	 *                         folders @param filterCriteria the filter
	 *                         criteria @return the folder @throws
	 *                         WorkspaceNavigatorServiceException the workspace
	 *                         navigator service exception @throws
	 * @param showableTypes    the showable types
	 * @param purgeEmpyFolders the purge empy folders
	 * @param filterCriteria   the filter criteria
	 * @return the folder
	 * @throws WorkspaceNavigatorServiceException the workspace navigator service
	 *                                            exception
	 */
	Item getFolder(Item folder, List<ItemType> showableTypes, boolean purgeEmpyFolders, FilterCriteria filterCriteria)
			throws WorkspaceNavigatorServiceException;

	/**
	 * Gets the root.
	 *
	 * @param showableTypes    the showable types
	 * @param purgeEmpyFolders the purge empy folders
	 * @param filterCriteria   the filter criteria
	 * @return the root
	 * @throws WorkspaceNavigatorServiceException the workspace navigator service
	 *                                            exception
	 */
	Item getRoot(List<ItemType> showableTypes, boolean purgeEmpyFolders, FilterCriteria filterCriteria)
			throws WorkspaceNavigatorServiceException;

	/**
	 * Gets the my special folder.
	 *
	 * @param showableTypes    the showable types
	 * @param purgeEmpyFolders the purge empy folders
	 * @param filterCriteria   the filter criteria
	 * @return the my special folder
	 * @throws WorkspaceNavigatorServiceException the workspace navigator service
	 *                                            exception
	 */
	Item getMySpecialFolder(List<ItemType> showableTypes, boolean purgeEmpyFolders, FilterCriteria filterCriteria)
			throws WorkspaceNavigatorServiceException;

	/**
	 * Gets the breadcrumbs by item identifier to parent limit.
	 *
	 * @param itemIdentifier      the item identifier
	 * @param parentLimit         the parent limit
	 * @param includeItemAsParent the include item as parent
	 * @return the breadcrumbs by item identifier to parent limit
	 * @throws Exception the exception
	 */
	List<Item> getBreadcrumbsByItemIdentifierToParentLimit(String itemIdentifier, String parentLimit,
			boolean includeItemAsParent) throws Exception;

	/**
	 * Gets the breadcrumbs by item identifier.
	 *
	 * @param itemId the item id
	 * @return the breadcrumbs by item identifier
	 * @throws Exception the exception
	 */
	// List<Item> getBreadcrumbsByItemIdentifier(String itemIdentifier, boolean
	// includeItemAsParent) throws Exception;

	/**
	 * Gets the public link for item id.
	 *
	 * @param itemId the item id
	 * @return the public link for item id
	 * @throws Exception the exception
	 */
	String getPublicLinkForItemId(String itemId) throws Exception;

	/**
	 * Gets the folder id from encrypted.
	 *
	 * @param encryptedFolderId the encrypted folder id
	 * @return the folder id from encrypted
	 * @throws Exception the exception
	 */
	String getFolderIdFromEncrypted(String encryptedFolderId) throws Exception;

	/**
	 * Gets the url by id.
	 *
	 * @param itemId the item id
	 * @return the url by id
	 * @throws Exception the exception
	 */
	String getUrlById(String itemId) throws Exception;

}
