/**
 * 
 */
package org.gcube.portlets.user.workspaceexplorerapp.shared;


/**
 * The Enum ItemType.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy)
 * Apr 9, 2020
 */
public enum ItemType {

	FOLDER,
	
	DOCUMENT,
	IMAGE_DOCUMENT,
	PDF_DOCUMENT,
	URL_DOCUMENT,
	METADATA,
	GCUBE_ITEM,
	
	UNKNOWN_TYPE
}
