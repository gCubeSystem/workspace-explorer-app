/**
 *
 */
package org.gcube.portlets.user.workspaceexplorerapp.server.workspace;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.http.HttpSession;

import org.gcube.common.storagehubwrapper.server.StorageHubWrapper;
import org.gcube.common.storagehubwrapper.server.tohl.Workspace;
import org.gcube.common.storagehubwrapper.shared.tohl.exceptions.InternalErrorException;
import org.gcube.common.storagehubwrapper.shared.tohl.items.URLFileItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class WsUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy) Apr 9, 2020
 */
public class WsInstanceUtil {

	public static final Logger logger = LoggerFactory.getLogger(WsInstanceUtil.class);
	public static final String ENVIRONMENT_VARIABLE_SCOPE_NAME = "EnvironmentVariableScope";
	public static final String ENVIRONMENT_VARIABLE_TOKEN_NAME = "EnvironmentVariableAppToken";
	public static final String SESSION_SCOPE = "session_scope";

	/**
	 * Gets the storage hub wrapper.
	 *
	 * @param httpSession the http session
	 * @return the storage hub wrapper
	 * @throws Exception the exception
	 */
	private static StorageHubWrapper getStorageHubWrapper(HttpSession httpSession) throws Exception {

		try {
			String scope = getScope(httpSession);
			String applicationToken = getApplicationToken(httpSession);

			if (applicationToken == null || applicationToken.isEmpty()) {
				throw new Exception("No Application Token found. Impossible to istance the SHUB");
			}

			return new StorageHubWrapper(scope, applicationToken, false, false, false);
		} catch (Exception e) {
			logger.error("Error when instancing the storageHub wrapper", e);
			throw new Exception("Error on inizializing the StorageHub wrapper. Please contact the support!");
		}
	}

	/**
	 * Gets the workpace.
	 *
	 * @param httpSession the http session
	 * @return the workpace
	 * @throws Exception the exception
	 */
	public static org.gcube.common.storagehubwrapper.server.tohl.Workspace getWorkspace(HttpSession httpSession)
			throws Exception {

		try {
			StorageHubWrapper wrapper = getStorageHubWrapper(httpSession);
			return wrapper.getWorkspace();
		} catch (Exception e) {
			logger.error("Error on getting the Workspace", e);
			throw new Exception("Error on getting the Workspace");
		}
	}

	/**
	 * Gets the scope.
	 *
	 * @param httpSession the http session
	 * @return the scope
	 */
	public static String getScope(HttpSession httpSession) {
		String scope = (String) httpSession.getAttribute(SESSION_SCOPE);
		logger.info("Variable " + SESSION_SCOPE + " read from httpsession is: " + scope);

		if (scope == null) {
			logger.info("Variable " + SESSION_SCOPE + " is null reading from web.xml context");
			String variableScopeName = httpSession.getServletContext()
					.getInitParameter(ENVIRONMENT_VARIABLE_SCOPE_NAME);
			logger.info("Found param-value '" + variableScopeName
					+ "' from web context, reading its value from ENVIRONMENT");
			scope = System.getenv(variableScopeName);
			logger.info("Value of " + variableScopeName + " from ENVIRONMENT is: " + scope);
		}

		httpSession.setAttribute(ENVIRONMENT_VARIABLE_SCOPE_NAME, scope);
		return scope;
	}

	/**
	 * Gets the application token.
	 *
	 * @param httpSession the http session
	 * @return the application token
	 */
	public static String getApplicationToken(HttpSession httpSession) {

		String appToken = null;
		String appTokenEnvVarName = null;
		try {
			logger.info("Reading " + ENVIRONMENT_VARIABLE_TOKEN_NAME + " from web.xml context");
			appTokenEnvVarName = httpSession.getServletContext().getInitParameter(ENVIRONMENT_VARIABLE_TOKEN_NAME);
			logger.info("Found param-value '" + appTokenEnvVarName
					+ "' from web context, reading its value from ENVIRONMENT");
			appToken = System.getenv(appTokenEnvVarName);
			logger.info("Value of " + appTokenEnvVarName + " from ENVIRONMENT is: " + appToken);

		} catch (Exception e) {
			logger.error("Error on reading the ENV VARIABLE '" + appTokenEnvVarName + "' from ENVIRONMENT");
		}

		return appToken;
	}

	/**
	 * Gets the url.
	 *
	 * @param workspace the workspace
	 * @param fileItem  the file item
	 * @return the url
	 * @throws Exception the exception
	 */
	public static String getUrl(Workspace workspace, URLFileItem fileItem) throws Exception {
		try {
			return readStreamAsString(
					workspace.downloadFile(fileItem.getId(), fileItem.getName(), null, null).getStream());
		} catch (IOException e) {
			logger.error("GET URL error for file: " + fileItem, e);
			throw new InternalErrorException(e.getMessage());
		}
	}

	/**
	 * Read the entire input stream as string. The system encoding is used.
	 *
	 * @param is the input stream.
	 * @return the read string.
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static String readStreamAsString(InputStream is) throws java.io.IOException {
		StringBuilder sb = new StringBuilder(1000);
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		char[] buf = new char[1024];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			sb.append(buf, 0, numRead);
		}
		reader.close();
		return sb.toString();
	}

}
