/**
 *
 */
package org.gcube.portlets.user.workspaceexplorerapp.client.rpc;

import java.util.List;

import org.gcube.portlets.user.workspaceexplorerapp.shared.FilterCriteria;
import org.gcube.portlets.user.workspaceexplorerapp.shared.Item;
import org.gcube.portlets.user.workspaceexplorerapp.shared.ItemType;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The Interface WorkspaceExplorerAppServiceAsync.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Jul 3, 2017
 */
public interface WorkspaceExplorerAppServiceAsync {

	/**
	 * Gets the public link for item id.
	 *
	 * @param itemId   the item id
	 * @param callback the callback
	 * @return the public link for item id
	 */
	void getPublicLinkForItemId(String itemId, AsyncCallback<String> callback);

	/**
	 * Gets the folder id from encrypted.
	 *
	 * @param encodedFolderID the encoded folder id
	 * @param callback        the callback
	 * @return the folder id from encrypted
	 */
	void getFolderIdFromEncrypted(String encodedFolderID, AsyncCallback<String> callback);

	/**
	 * Gets the root.
	 *
	 * @param showableTypes    the showable types
	 * @param purgeEmpyFolders the purge empy folders
	 * @param filterCriteria   the filter criteria
	 * @param callback         the callback
	 * @return the root
	 */
	public void getRoot(List<ItemType> showableTypes, boolean purgeEmpyFolders, FilterCriteria filterCriteria,
			AsyncCallback<Item> callback);

	/**
	 * Gets the folder.
	 *
	 * @param folder           the folder
	 * @param showableTypes    the showable types
	 * @param purgeEmpyFolders the purge empy folders
	 * @param filterCriteria   the filter criteria
	 * @param callback         the callback
	 * @return the folder
	 */
	public void getFolder(Item folder, List<ItemType> showableTypes, boolean purgeEmpyFolders,
			FilterCriteria filterCriteria, AsyncCallback<Item> callback);

	/**
	 * Gets the breadcrumbs by item identifier.
	 *
	 * @param showableTypes    the showable types
	 * @param purgeEmpyFolders the purge empy folders
	 * @param filterCriteria   the filter criteria
	 * @param asyncCallback    the async callback
	 * @return the breadcrumbs by item identifier
	 */
	// public void getBreadcrumbsByItemIdentifier(String itemIdentifier, boolean
	// includeItemAsParent, AsyncCallback<List<Item>> asyncCallback);

	/**
	 * Gets the my special folder.
	 *
	 * @param showableTypes    the showable types
	 * @param purgeEmpyFolders the purge empy folders
	 * @param filterCriteria   the filter criteria
	 * @param asyncCallback    the async callback
	 * @return the my special folder
	 */
	public void getMySpecialFolder(List<ItemType> showableTypes, boolean purgeEmpyFolders,
			FilterCriteria filterCriteria, AsyncCallback<Item> asyncCallback);

	/**
	 * Gets the breadcrumbs by item identifier to parent limit.
	 *
	 * @param itemIdentifier      the item identifier
	 * @param parentLimit         the parent limit
	 * @param includeItemAsParent the include item as parent
	 * @param callback            the callback
	 * @return the breadcrumbs by item identifier to parent limit
	 */
	void getBreadcrumbsByItemIdentifierToParentLimit(String itemIdentifier, String parentLimit,
			boolean includeItemAsParent, AsyncCallback<List<Item>> callback);

	/**
	 * Gets the url by id.
	 *
	 * @param itemId   the item id
	 * @param callback the callback
	 * @return the url by id
	 */
	void getUrlById(String itemId, AsyncCallback<String> callback);

}
