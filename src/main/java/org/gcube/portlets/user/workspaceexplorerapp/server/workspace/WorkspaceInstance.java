package org.gcube.portlets.user.workspaceexplorerapp.server.workspace;

import java.io.Serializable;

import javax.servlet.http.HttpSession;

import org.gcube.common.storagehubwrapper.server.tohl.Workspace;

/**
 * The Class WorkspaceInstance.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy)
 * Apr 9, 2020
 */
public class WorkspaceInstance implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5109673588015843212L;
	
	private HttpSession httpSession;
	private Workspace workspace;

	
	/**
	 * Instantiates a new workspace instance.
	 *
	 * @param session the session
	 */
	public WorkspaceInstance(HttpSession session) {
		this.httpSession = session;
	}
	
	/**
	 * Gets the.
	 *
	 * @return the workspace
	 * @throws Exception the exception
	 */
	public Workspace get() throws Exception{
		if(workspace==null)
			workspace = WsInstanceUtil.getWorkspace(httpSession);
		
		return workspace;
	}

}
