/**
 *
 */
package org.gcube.portlets.user.workspaceexplorerapp.server.workspace;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.gcube.common.storagehubwrapper.shared.tohl.WorkspaceFolder;
import org.gcube.common.storagehubwrapper.shared.tohl.WorkspaceItem;
import org.gcube.common.storagehubwrapper.shared.tohl.WorkspaceSharedFolder;
import org.gcube.common.storagehubwrapper.shared.tohl.exceptions.InternalErrorException;
import org.gcube.common.storagehubwrapper.shared.tohl.items.FileItem;
import org.gcube.common.storagehubwrapper.shared.tohl.items.PropertyMap;
import org.gcube.portlets.user.workspaceexplorerapp.client.Util;
import org.gcube.portlets.user.workspaceexplorerapp.client.WorkspaceExplorerAppConstants;
import org.gcube.portlets.user.workspaceexplorerapp.shared.FilterCriteria;
import org.gcube.portlets.user.workspaceexplorerapp.shared.Item;
import org.gcube.portlets.user.workspaceexplorerapp.shared.ItemType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


// TODO: Auto-generated Javadoc
/**
 * The Class ItemBuilder.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Feb 22, 2016
 */
public class ItemBuilder {

	public static final Logger _log = LoggerFactory.getLogger(ItemBuilder.class);
	/**
	 * Purge empty folders.
	 *
	 * @param item the item
	 * @return the item
	 */
	public static Item purgeEmptyFolders(Item item)
	{
		//for (Item child:item.getChildren()) purgeEmptyFolders(child); ONLY FIRST LEVEL

		List<Item> toRemoveList = new LinkedList<Item>();
		for (Item child:item.getChildren()) {
			boolean toRemove = isAnEmptyFolder(child);
			if (toRemove) {
				toRemoveList.add(child);
			}
		}
		for (Item child:toRemoveList) {
			item.removeChild(child);
		}

		return item;
	}


	/**
	 * Checks if is an empty folder.
	 *
	 * @param item the item
	 * @return true, if is an empty folder
	 */
	protected static boolean isAnEmptyFolder(Item item)
	{
		return Util.isFolder(item.getType()) && item.getChildren().size() == 0;
	}


	/**
	 * Gets the item.
	 *
	 * @param parent the parent
	 * @param workspaceItem the workspace item
	 * @param workspaceItemPath the workspace item path
	 * @param showableTypes the showable types
	 * @param filterCriteria the filter criteria
	 * @param loadChildren the load children
	 * @return the item
	 * @throws InternalErrorException the internal error exception
	 */
	public static Item getItem(Item parent, WorkspaceItem workspaceItem, String workspaceItemPath, List<ItemType> showableTypes, FilterCriteria filterCriteria) throws Exception
	{

		try {
			
			String itemName =  workspaceItem.getName();
			boolean isFolder = workspaceItem.isFolder();
			ItemType type = getItemType(workspaceItem);
	
			if (!showableTypes.contains(type)) {
				return null;
			}
			if (!filterItem(workspaceItem, filterCriteria)) {
				return null;
			}
	
			boolean isSharedFolder = false;
			if(workspaceItem instanceof WorkspaceSharedFolder){
				isSharedFolder = true;
				_log.debug("Is shared folder");
				WorkspaceSharedFolder shared = (WorkspaceSharedFolder) workspaceItem;
				//itemName = shared.isVreFolder()?shared.getDisplayName():workspaceItem.getName(); //NOT IMPLEMENTED BY SHUB
				itemName = shared.isVreFolder()?shared.getName():workspaceItem.getName();
			}
	
	//		_log.debug("Building Item for: "+itemName);
			Item item = null;
			try{
	
				item = new Item(parent, 
						workspaceItem.getId(), 
						itemName, 
						type,
						workspaceItemPath, 
						workspaceItem.getOwner(), 
						toDate(workspaceItem.getCreationTime()), 
						isFolder, 
						false);
				
				item.setSharedFolder(isSharedFolder);
			}catch(Exception e){
				_log.error("Error on getting item: "+itemName+" with id: "+workspaceItem.getId()+", from HL, so skipping item");
				return null;
			}
	
//			if(loadChildren){
//				//TODO A PATCH TO AVOID A SLOW GETPATH
//	//			workspaceItemPath = workspaceItem.getPath();
//				for (WorkspaceItem child: workspaceItem.getChildren()){
//					String itemPath = workspaceItemPath;
//					if(child.isFolder())
//						itemPath+="/"+child.getName();
//	
//	//				_log.trace("\nConverting child item: "+child.getName());
//					Item itemChild = getItem(item, child, itemPath, showableTypes, filterCriteria, false);
//	//				_log.trace("Item: "+child.getName() +" converted!!!");
//					if (itemChild!=null){
//						item.addChild(itemChild);
//					}
//				}
//			}

			return item;
		}catch (Exception e) {
			_log.error("Error on getting item: "+workspaceItem.getName()+" with id: "+workspaceItem.getId(), e);
			throw new Exception("Error on retrieving the item: "+workspaceItem.getName());
		}
	}
	
	
	/**
	 * Adds the children.
	 *
	 * @param item the item
	 * @param itemPathBuilt the item path built
	 * @param children the children
	 * @param showableTypes the showable types
	 * @param filterCriteria the filter criteria
	 * @return the item with the children added
	 * @throws Exception the exception
	 */
	public static Item addChildren(Item item, String itemPathBuilt, List<? extends WorkspaceItem> children, List<ItemType> showableTypes, FilterCriteria filterCriteria) throws Exception {
		
		if(item==null)
			return null;
		
		if(children==null)
			return item;
		
		for (WorkspaceItem child: children){
			String itemPath = itemPathBuilt;
			if(child.isFolder()) {
				itemPath+="/"+child.getName();
			}

			Item itemChild = getItem(item, child, itemPath, showableTypes, filterCriteria);
//				_log.trace("Item: "+child.getName() +" converted!!!");
			if (itemChild!=null){
				item.addChild(itemChild);
			}
		}
		
		return item;
	}


	/**
	 * To date.
	 *
	 * @param calendar the calendar
	 * @return the date
	 */
	public static  Date toDate(Calendar calendar)
	{
		if (calendar == null) return null;
		return calendar.getTime();

	}

	/**
	 * Gets the item type.
	 *
	 * @param item the item
	 * @return the item type
	 */
	protected static ItemType getItemType(WorkspaceItem item) {
		switch (item.getType()) {
		case SHARED_FOLDER:
		case FOLDER: {
			return ItemType.FOLDER;
		}
		case FILE_ITEM:

			if (item instanceof FileItem)

				return getFileItemType((FileItem) item);

			else
				return ItemType.UNKNOWN_TYPE;
		case URL_ITEM:
			return ItemType.URL_DOCUMENT;

		default:
			return null;
		}
	}

	
	/**
	 * Gets the file item type.
	 *
	 * @param item the item
	 * @return the file item type
	 */
	protected static ItemType getFileItemType(FileItem item){

		try{
			return ItemType.valueOf(item.getFileItemType().toString());
		}catch (Exception e) {
			_log.error("Item Type non found: ",e);
			return ItemType.UNKNOWN_TYPE;
		}
	}

	
	/**
	 * Filter item using the input Filter Criteria
	 *
	 * @param item the item
	 * @param filterCriteria the filter criteria
	 * @return true, if successful
	 */
	protected static boolean filterItem(WorkspaceItem item, FilterCriteria filterCriteria) {
		if(filterCriteria==null) {
			return true;
		}

		boolean mimeTypeCheck = checkAllowedMimeTypes(item, filterCriteria.getAllowedMimeTypes());
		if (!mimeTypeCheck) {
			return false;
		}

		boolean fileExtensionCheck = checkAllowedFileExtension(item, filterCriteria.getAllowedFileExtensions());
		if(!fileExtensionCheck) {
			return false;
		}

		boolean propertiesCheck = checkProperties(item, filterCriteria.getRequiredProperties());
		
		return propertiesCheck;
	}

	/**
	 * Check allowed mime types on file.
	 *
	 * @param item the item
	 * @param allowedMimeTypes the allowed mime types
	 * @return true, if successful
	 */
	protected static boolean checkAllowedMimeTypes(WorkspaceItem item, List<String> allowedMimeTypes){
		if (allowedMimeTypes==null || allowedMimeTypes.size()==0) {
			return true;
		}

		if (item instanceof FileItem) {
			FileItem fileItem = (FileItem) item;
			String mimeType = fileItem.getMimeType();
			return allowedMimeTypes.contains(mimeType);
		}
		return true;
	}




	/**
	 * Check allowed file extension on file.
	 *
	 * @param item the item
	 * @param allowedFileExtension the allowed mime types
	 * @return true, if successful
	 */
	protected static boolean checkAllowedFileExtension(WorkspaceItem item, List<String> allowedFileExtension){
		if (allowedFileExtension==null || allowedFileExtension.size()==0) {
			return true;
		}

		try {
			if (item instanceof FileItem) {
				String name = item.getName();
				return checkFileExtension(name, allowedFileExtension);
			}
			return true;
		} catch (Exception e) {
			_log.error("checkAllowedFileExtension, InternalErrorException: ",e);
			return false;
		}
	}

	/**
	 * Check file extension.
	 *
	 * @param fileName the file name
	 * @param allowedFileExtension the allowed file extension
	 * @return true, if successful
	 */
	protected static boolean checkFileExtension(String fileName, List<String> allowedFileExtension){

		if(fileName==null || fileName.isEmpty()) {
			return false;
		}

		int dot = fileName.lastIndexOf(".");
		if(dot>=0 && dot+1<=fileName.length()){

			String ext = fileName.substring(dot+1, fileName.length());
			_log.trace("Extension found: "+ext +" for: "+fileName);
//			if(ext.isEmpty())
//				return false;
			for (String fe : allowedFileExtension) {
				if(ext.compareTo(fe)==0) {
					return true;
				}
			}
			return false;
		}
		_log.trace("Extension not found for: "+fileName);
		return false;
	}


	/**
	 * Check properties.
	 *
	 * @param item the item
	 * @param requestedProperties the requested properties
	 * @return true, if successful
	 */
	protected static boolean checkProperties(WorkspaceItem item, Map<String, String> requestedProperties) {
		
		if (requestedProperties == null || requestedProperties.size() == 0) {
			return true;
		}

		if (item.getPropertyMap() != null) {
			PropertyMap mapProp = item.getPropertyMap();
			if (mapProp != null && mapProp.getValues() != null) {
				Map<String, Object> itemProperties = mapProp.getValues();
				for (Entry<String, String> requestProperty : requestedProperties.entrySet()) {
					Object propertyValue = itemProperties.get(requestProperty.getKey());
					if (propertyValue == null) {
						return false;
					}
					if (!propertyValue.equals(requestProperty.getValue())) {
						return false;
					}
				}
			}
		}

		return true;
	}



	/**
	 * Builds the folder to breadcrumbs.
	 *
	 * @param wsFolder the ws folder
	 * @param parent the parent
	 * @return the item
	 * @throws InternalErrorException the internal error exception
	 */
	public static Item buildFolderForBreadcrumbs(WorkspaceFolder wsFolder, Item parent) throws Exception {

		String name = "";
		boolean isSpecialFolder = false;
		boolean isRoot = false;

		if(wsFolder.isRoot()){ //IS ROOT
			name = WorkspaceExplorerAppConstants.HOME_LABEL;
			isRoot = true;
		}else if(wsFolder.isShared() && wsFolder instanceof WorkspaceSharedFolder){ 
			//MANAGEMENT SHARED FOLDER NAME
	    	WorkspaceSharedFolder shared = (WorkspaceSharedFolder) wsFolder;
	    	//name = shared.isVreFolder()?shared.getDisplayName():wsFolder.getName();
	    	name = shared.isVreFolder()?shared.getName():wsFolder.getName(); //not implemented by SHUB
	    	
		}else if(parent!=null && isSpecialFolder(wsFolder, parent.isRoot())){
			//MANAGEMENT SPECIAL FOLDER
			name = WorkspaceExplorerAppConstants.VRE_FOLDERS_LABEL;
			isSpecialFolder = true;
		}
		else {
			name = wsFolder.getName();
		}

		//BUILDS A SIMPLE ITEM FOR BREADCRUMB
		String path = null; //wsFolder.getPath(); FORCED TO NULL BECAUSE IS SLOW CALL
		Item item = new Item(null, wsFolder.getId(), name, ItemType.FOLDER, path, null, null, true, isRoot);
		item.setSpecialFolder(isSpecialFolder);

		_log.debug("breadcrumb returning: "+item);
		return item;
	}

	/**
	 * Checks if is special folder.
	 *
	 * @param wsFolder the ws folder
	 * @return true, if is special folder
	 */
	public static boolean isSpecialFolder(WorkspaceFolder wsFolder, boolean parentIsRoot){

		try {
			return wsFolder.getName().compareTo(WorkspaceExplorerAppConstants.SPECIAL_FOLDERS_NAME)==0 && wsFolder.getParentId()!=null && parentIsRoot;
		} catch (Exception e) {
			_log.warn("isSpecialFolder exception, returning false");
			return false;
		}
	}
	/*
	public static void main(String[] args) {
		List<String> allowedFileExtension = new ArrayList<String>();
		allowedFileExtension.add("csv");
		allowedFileExtension.add("");

		String fileName = "t";
		System.out.println(checkFileExtension(fileName, allowedFileExtension));
	}*/
}
