package org.gcube.portlets.user.workspaceexplorerapp.server;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpSession;

import org.gcube.common.encryption.StringEncrypter;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.common.storagehubwrapper.server.tohl.Workspace;
import org.gcube.common.storagehubwrapper.shared.tohl.WorkspaceFolder;
import org.gcube.common.storagehubwrapper.shared.tohl.WorkspaceItem;
import org.gcube.common.storagehubwrapper.shared.tohl.WorkspaceItemType;
import org.gcube.common.storagehubwrapper.shared.tohl.items.FileItem;
import org.gcube.common.storagehubwrapper.shared.tohl.items.URLFileItem;
import org.gcube.common.storagehubwrapper.shared.tohl.items.URLItem;
import org.gcube.portlets.user.workspaceexplorerapp.client.WorkspaceExplorerAppConstants;
import org.gcube.portlets.user.workspaceexplorerapp.client.rpc.WorkspaceExplorerAppService;
import org.gcube.portlets.user.workspaceexplorerapp.server.workspace.ItemBuilder;
import org.gcube.portlets.user.workspaceexplorerapp.server.workspace.WorkspaceInstance;
import org.gcube.portlets.user.workspaceexplorerapp.server.workspace.WsInstanceUtil;
import org.gcube.portlets.user.workspaceexplorerapp.shared.FilterCriteria;
import org.gcube.portlets.user.workspaceexplorerapp.shared.Item;
import org.gcube.portlets.user.workspaceexplorerapp.shared.ItemType;
import org.gcube.portlets.user.workspaceexplorerapp.shared.WorkspaceNavigatorServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


// TODO: Auto-generated Javadoc
/**
 * The Class WorkspaceExplorerAppServiceImpl.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jul 3, 2017
 */
@SuppressWarnings("serial")
public class WorkspaceExplorerAppServiceImpl extends RemoteServiceServlet implements WorkspaceExplorerAppService {

	public static final String THE_WORKSPACE_SHUB_INSTANCIED = "THE_WORKSPACE_SHUB_INSTANCIED";
	/**
	 *
	 */
	public static final Logger logger = LoggerFactory.getLogger(WorkspaceExplorerAppServiceImpl.class);
	public static final String UTF_8 = "UTF-8";

	
	/**
	 * Gets the workspace.
	 *
	 * @return the workspace
	 * @throws Exception 
	 */
	private Workspace getWorkspace() throws Exception {
		HttpSession httpSession = this.getThreadLocalRequest().getSession();
		Workspace workspace = (Workspace) httpSession.getAttribute(THE_WORKSPACE_SHUB_INSTANCIED);
		
		if(workspace==null) {
			WorkspaceInstance workspaceInstance = new WorkspaceInstance(httpSession);
			workspace = workspaceInstance.get();
			httpSession.setAttribute(THE_WORKSPACE_SHUB_INSTANCIED, workspace);
		}
		
		return workspace;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Item getRoot(List<ItemType> showableTypes, boolean purgeEmpyFolders, FilterCriteria filterCriteria) throws WorkspaceNavigatorServiceException {
		logger.info("getRoot showableTypes: "+showableTypes+" purgeEmpyFolders: "+purgeEmpyFolders+" filterCriteria: "+ filterCriteria);

		try {

			Workspace workspace = getWorkspace();
			logger.trace("Start getRoot...");

			org.gcube.common.storagehubwrapper.shared.tohl.WorkspaceFolder root = workspace.getRoot();

			logger.trace("GetRoot  - Replyiing root");
			long startTime = System.currentTimeMillis();
			logger.trace("start time - " + startTime);

			
			Item rootItem = ItemBuilder.getItem(null, root, root.getPath(), showableTypes, filterCriteria);
			//adding the children
			List<? extends WorkspaceItem> children = workspace.getChildren(root.getId());
			rootItem = ItemBuilder.addChildren(rootItem, "", children, showableTypes, filterCriteria);
			rootItem.setName(WorkspaceExplorerAppConstants.HOME_LABEL);
			rootItem.setIsRoot(true);

			/* SPECIAL FOLDERS
			Item specialFolders = ItemBuilder.getItem(null, specials, showableTypes, filterCriteria, 2);
			specialFolders.setShared(true);
			rootItem.addChild(specialFolders);
			 */
			if (purgeEmpyFolders) {
				rootItem = ItemBuilder.purgeEmptyFolders(rootItem);
			}

			logger.trace("Returning:");
			Long endTime = System.currentTimeMillis() - startTime;
			String time = String.format("%d msc %d sec", endTime, TimeUnit.MILLISECONDS.toSeconds(endTime));
			logger.info("getRoot end time - " + time);

			Collections.sort(rootItem.getChildren(), new ItemComparator());
			logger.info("Returning children size: "+rootItem.getChildren().size());

			return rootItem;

		} catch (Exception e) {
			logger.error("Error during root retrieving", e);
			throw new WorkspaceNavigatorServiceException("Sorry, an error occurred when performing get root");
		}
	}


	/**
	 * Gets the folder.
	 *
	 * @param item the item
	 * @param showableTypes the showable types
	 * @param purgeEmpyFolders the purge empy folders
	 * @param filterCriteria the filter criteria
	 * @return the folder
	 * @throws WorkspaceNavigatorServiceException the workspace navigator service exception
	 */
	@Override
	public Item getFolder(Item item, List<ItemType> showableTypes, boolean purgeEmpyFolders, FilterCriteria filterCriteria) throws WorkspaceNavigatorServiceException {
		logger.info("called getFolder item: "+item+" showableTypes: "+showableTypes+" purgeEmpyFolders: "+purgeEmpyFolders+" filterCriteria: "+filterCriteria);

		WorkspaceItem workspaceItem = null;
		Workspace workspace = null;
		try {

			if(item==null || item.getId()==null)
				throw new Exception("Item id is null");

			workspace = getWorkspace();
			workspaceItem = workspace.getItemForExplorer(item.getId(), false, false, false);
			
		} catch (Exception e) {
			logger.error("Error during item retrieving", e);
			
			if(e.getMessage()!=null && e.getMessage().startsWith("Insufficent Privileges")) {
				String erroMsg = item.getName()!=null?"The folder '"+item.getName()+"'":"This folder";
				erroMsg+= " is not accessible as an anonymous user!";
				throw new WorkspaceNavigatorServiceException(erroMsg);
			}
			
			throw new WorkspaceNavigatorServiceException("Sorry, an error occurred when performing get folder. Does it still exist?");
		}
			
		if(workspaceItem != null && workspaceItem.isFolder()) {
			
			try {
				
				WorkspaceItem folder = workspaceItem;
	
				logger.trace("GetFolder - Replyiing folder");
				long startTime = System.currentTimeMillis();
				logger.trace("start time - " + startTime);
	
				String path = item.getPath()!=null && !item.getPath().isEmpty()?item.getPath():folder.getPath();
				Item itemFolder = ItemBuilder.getItem(null, folder, path, showableTypes, filterCriteria);
				//adding the children
				List<? extends WorkspaceItem> children = workspace.getChildren(itemFolder.getId());
				itemFolder = ItemBuilder.addChildren(itemFolder, path, children, showableTypes, filterCriteria);
	//			_log.trace("Only showable types:");
	
				if (purgeEmpyFolders) {
					itemFolder = ItemBuilder.purgeEmptyFolders(itemFolder);
				}
	
				logger.trace("Returning:");
				Long endTime = System.currentTimeMillis() - startTime;
				String time = String.format("%d msc %d sec", endTime, TimeUnit.MILLISECONDS.toSeconds(endTime));
				logger.info("getFolder end time - " + time);
	
				Collections.sort(itemFolder.getChildren(), new ItemComparator());
	
				return itemFolder;
			}catch (Exception e) {
				throw new WorkspaceNavigatorServiceException(e.getMessage());
			}
			
		}else {
			logger.error("The item requested is not a folder");
			throw new WorkspaceNavigatorServiceException("Error, the item requested is not a folder");
		}
				


	}
	

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Item getMySpecialFolder(List<ItemType> showableTypes, boolean purgeEmpyFolders, FilterCriteria filterCriteria) throws WorkspaceNavigatorServiceException {
		logger.info("called getMySpecialFolder showableTypes: "+showableTypes+" purgeEmpyFolders: "+purgeEmpyFolders+" filterCriteria: "+filterCriteria);
		throw new WorkspaceNavigatorServiceException("The method getMySpecialFolder is no longer maintained");
		
		/*try {

			Workspace workspace = getWorkspace()
			WorkspaceItem folder = workspace.getMySpecialFolders();
			long startTime = System.currentTimeMillis();
			
			logger.trace("start time - " + startTime);
			Item itemFolder = ItemBuilder.getItem(null, folder, folder.getPath(), showableTypes, filterCriteria);
			//adding the children
			List<? extends WorkspaceItem> children = workspace.getChildren(itemFolder.getId());
			itemFolder = ItemBuilder.addChildren(itemFolder, "", children, showableTypes, filterCriteria);
			
			//OVERRIDING VRE FOLDERS NAME - SET SPECIAL FOLDER  /Workspace/MySpecialFolders
			itemFolder.setName(WorkspaceExplorerAppConstants.VRE_FOLDERS_LABEL);
			itemFolder.setSpecialFolder(true);
			logger.trace("Built MySpecialFolder: "+itemFolder);
			logger.trace("Only showable types:");
			if (purgeEmpyFolders) {
				itemFolder = ItemBuilder.purgeEmptyFolders(itemFolder);
			}
			
			logger.trace("Returning:");
			Long endTime = System.currentTimeMillis() - startTime;
			String time = String.format("%d msc %d sec", endTime, TimeUnit.MILLISECONDS.toSeconds(endTime));
			logger.info("getMySpecialFolder end time - " + time);
			Collections.sort(itemFolder.getChildren(), new ItemComparator());
			return itemFolder;

		} catch (Exception e) {
			logger.error("Error during special folders retrieving", e);
			throw new WorkspaceNavigatorServiceException("Sorry, an error occurred when performing get My Special Folder");
		}*/
	}

	/**
	 * Gets the parents by item identifier to limit.
	 * when limit is reached parents are null
	 * @param itemIdentifier            the item identifier
	 * @param parentLimit the parent limit
	 * @param includeItemAsParent            the include item as parent
	 * @return the parents by item identifier to limit
	 * @throws Exception             the exception
	 */
	@Override
	public List<Item> getBreadcrumbsByItemIdentifierToParentLimit(String itemIdentifier, String parentLimit, boolean includeItemAsParent) throws Exception {
		logger.info("called getBreadcrumbsByItemIdentifierToParentLimit by Item Identifier " + itemIdentifier +" and limit: "+parentLimit + " and includeItemAsParent: "+includeItemAsParent);
		try {

			Workspace workspace = getWorkspace();
			WorkspaceItem wsItem = workspace.getItemForExplorer(itemIdentifier,false,false,false);
			logger.info("workspace item returned has name: "+wsItem.getName());
			List<? extends WorkspaceItem> parents = null;
			try{
				parents = workspace.getParentsForExplorerById(itemIdentifier);
			}catch(Exception e){
				logger.error("Error on getting list of parents from SHUB for id: "+itemIdentifier, e);
				parents = new ArrayList<WorkspaceItem>();
			}

			if(parents==null) //avoiding null
				parents = new ArrayList<WorkspaceItem>();

			if(logger.isDebugEnabled()) {
				for (WorkspaceItem workspaceItem : parents) {
					logger.debug("the parent is: "+workspaceItem.getName());
				}
			}
			
			
			List<Item> arrayParents = new ArrayList<Item>();
			parentLimit = parentLimit!=null?parentLimit:"";

			logger.info("parent size returned by SHUB is: "+parents.size());
			
			switch (parents.size()) {
			case 0: // itemIdentifier is ROOT
				logger.trace("itemIdentifier isRoot...");
				if (includeItemAsParent) { //ADDIND ROOT
					WorkspaceFolder wsFolder =(WorkspaceFolder) wsItem;
					Item root = ItemBuilder.buildFolderForBreadcrumbs(wsFolder, null);
					List<Item> listParents = new ArrayList<Item>(1);
					listParents.add(root);
					//					workspaceLogger.trace("returning: "+listParents.toString());
					return listParents;
				}
				else{
					logger.trace("returning empty list");
					return new ArrayList<Item>(); // empty list
				}
			}

			//Adding the item passed as last first parent
			if(includeItemAsParent && wsItem.isFolder()) {
				WorkspaceFolder wsItemAsFolder =(WorkspaceFolder) wsItem;
				Item theItem = ItemBuilder.buildFolderForBreadcrumbs(wsItemAsFolder, null);
				arrayParents.add(theItem);
			}

			/** HANDLE MY_SPECIAL_FOLDER TO AVOID COMPLETE PATH WORKSPACE/MY_SPECIAL_FOLDER
			 * BUT RETURNING ONLY /MY_SPECIAL_FOLDER
			 */
			
			WorkspaceItem theFirstParent =  parents.get(0);
			
			if(wsItem.isFolder()){
				if(ItemBuilder.isSpecialFolder((WorkspaceFolder) wsItem, theFirstParent.isRoot())){
					logger.debug("item id is special folder, returning");
					return new ArrayList<Item>(arrayParents);
				}

				if(itemIdentifier.compareTo(parentLimit)==0){
					logger.debug("item and parent limit are identical element, returning");
					return new ArrayList<Item>(arrayParents);
				}

			}

			//CONVERTING PATH
			logger.debug("converting parents...");
			//for (int i =  parents.size()-2; i >= 0; i--) {
			for (int i =  0; i<parents.size(); i++) {
				logger.debug("converting index: "+i);
				WorkspaceFolder wsParentFolder = (WorkspaceFolder) parents.get(i);
				logger.debug("the folder at index: "+i +" is "+wsParentFolder.getName());
				Item itemParent = ItemBuilder.buildFolderForBreadcrumbs(wsParentFolder, null);
				arrayParents.add(itemParent);
				if(itemParent.isSpecialFolder()){ //SKIP HOME PARENT FOR MY_SPECIAL_FOLDER
					logger.info("arrayParents index "+i+" is special folder, break");
					break;
				}else if(parentLimit.compareTo(itemParent.getId())==0){
					logger.info("reached parent limit "+parentLimit+", break");
					break;
				}
			}

			//SET PARENTS
			logger.debug("setting parents..");
			for(int i=0; i<arrayParents.size()-1; i++){

				Item parent = arrayParents.get(i+1);
				Item fileModel = arrayParents.get(i);

				if(fileModel!=null) {
					fileModel.setParent(parent);
				}
			}
			
			Collections.reverse(arrayParents);
			logger.debug("Reversed the parent list");
			
			if(logger.isDebugEnabled()) {
				for (Item item : arrayParents) {
					logger.debug("Returning parent: "+item.getName());
				}
			}
			return arrayParents;
			
		} catch (Exception e) {
			logger.error("Error in get List Parents By Item Identifier ", e);
			throw new Exception("Sorry, I'm not able to load the location of the folder. Does it still exist?");
		}
	}

	/**
	 * Checks if is a shared folder.
	 *
	 * @param item the item
	 * @param asRoot the as root
	 * @return true, if is a shared folder
	 */
	private boolean isASharedFolder(WorkspaceItem item, boolean asRoot) {
		try {

			if (item != null && item.isFolder() && item.isShared()) { // IS A SHARED SUB-FOLDER
				if (asRoot) {
					return item.getType().equals(WorkspaceItemType.SHARED_FOLDER); // IS ROOT?
				}

				return true;
			}
			return false;
		} catch (Exception e) {
			logger.error("Error in server isASharedFolder", e);
			return false;
		}
	}


	/**
	 * Gets the public link for item id.
	 *
	 * @param itemId the item id
	 * @return the public link for item id
	 * @throws Exception the exception
	 */
	@Override
	public String getPublicLinkForItemId(String itemId) throws Exception{
		logger.info("calle getPublicLinkForItemId: "+ itemId);
		
		try{

			if(itemId==null)
				throw new Exception("Sorry, the itemId is null. The public link for empty item is unavailable");

			Workspace workspace = getWorkspace();
			WorkspaceItem wsItem = workspace.getItemForExplorer(itemId,false,false,false);

			if(wsItem==null)
				throw new Exception("Sorry, the workspace item is null. The public link for empty item is unavailable");

			if(wsItem instanceof FileItem){

				URL theURL = workspace.getPublicLinkForFile(wsItem.getId());

				if(theURL==null)
					throw new Exception("Sorry, the public link for "+wsItem.getName() +" is not available");
				
				return theURL.toString();

			}else{
				logger.warn("ItemId: "+ itemId +" is not available, sent exception Public Link unavailable");
				throw new Exception("Sorry, the Public Link for selected item is unavailable");
			}

		}catch (Exception e) {
			logger.error("Error on getting public link for item: "+itemId, e);
			throw new Exception(e.getMessage());
		}

	}

	/**
	 * Gets the valid id from encrypted.
	 *
	 * @param encodedFolderId the encrypted folder id
	 * @return the valid id from encrypted
	 * @throws Exception the exception
	 */
	@Override
	public String getFolderIdFromEncrypted(String encodedFolderId) throws Exception{
		try{
			String scope = WsInstanceUtil.getScope(this.getThreadLocalRequest().getSession());
			ScopeProvider.instance.set(scope);
			logger.info("Trying to decode encoded folder Id: "+encodedFolderId +" in the scope: "+scope);
			String base64DecodedId = StringUtil.base64DecodeString(encodedFolderId);
//			String useThis = "P+IpJ6F6cTaGENfKMQWmStGUE79gbri5bVGRnzOvb8YUNIsJqFrdhceBrF+/u00j";
			logger.info("Base 64 decoded folder Id: "+base64DecodedId +", now decrypting...");
			String decryptedFId = StringEncrypter.getEncrypter().decrypt(base64DecodedId);
			logger.info("Decrypted folder Id: "+decryptedFId, " returning");
			return decryptedFId;
		}catch(Exception e){
			logger.error("Error during decrypting folder Id: "+encodedFolderId,e);
			throw new Exception("Sorry, an error occurred when decrypting the folder id. Try again or contact the support");
		}
	}
	
	@Override
	public String getUrlById(String itemId) throws Exception {

		try {

			if (itemId == null)
				throw new Exception("Workspace itemId is null");

			logger.debug("get URL by id: " + itemId);
			Workspace workspace = getWorkspace();
			WorkspaceItem wsItem = workspace.getItemForExplorer(itemId, false, false, false);
			// workspace

			if (wsItem instanceof org.gcube.common.storagehubwrapper.shared.tohl.impl.URLFileItem) {
				URLFileItem fileItem = (URLFileItem) wsItem;
				return WsInstanceUtil.getUrl(workspace, fileItem);
			} else if (wsItem instanceof URLItem) {
				URLItem urlFile = (URLItem) wsItem;
				return urlFile.getValue().toString();
			} else {
				throw new Exception("The itemId " + itemId + " is not a URLFileItem or a URLItem");
			}

		} catch (Exception e) {
			logger.error("Error occurred in getUrlById: ", e);
			throw new Exception(e.getMessage());
		}
	}

}
