
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.4.2] - 2024-09-17

- [#28026] Moved to `gcube-smartgears-bom 2.5.1{-SNAPSHOT}` in order to use SHUB.2.X in PROD
- v1.4.1-SNAPSHOT - Moved to SHUB 2.x through the storagehub-client-wrapper 1.1.1 [#22783]

#### Enhancements

[#22782] Moved to SHUB(1.x, 3.x)

## [v1.4.0] - 2021-12-03

- [#19786] Integrated items of type "URL"

## [v1.3.1] - 2020-09-25

- [#19317] Just to include the storagehub-client-wrapper at 1.0.0 version

#### Fixes

[#19671#note-10] Resolve public link on private folder with at least one shared folder as descendant


## [v1.3.0] - 2020-04-14

[Task #18920] Migrated to SHUB


## [v1.2.0] - 2018-03-07

[Feature #11374] Download items with double click


## [v1.1.0] - 2017-10-11

[Feature #9926] read the scope from environment variable


## [v1.0.2] - 2017-10-10

Removed markAsRead method. It is performed by HL


## [v1.0.1] - 2017-07-03

[Task #9104] removed log4j.properties file


## [v.0.0.1] - 2016-11-10

Incident #5722: fixed


## [v1.0.0] - 2016-09-13

first release
